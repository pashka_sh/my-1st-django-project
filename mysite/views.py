from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.generic.base import TemplateView

from mysite.models import Dish


class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = {
            'dishes': Dish.objects.all()
        }
        return context





class Page1View(TemplateView):
    template_name = "page1.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page1', 'Pasha', 'Nika', 'Argen'],
            'title': "My students",
        }
        return context


class Page2View(TemplateView):
    template_name = "page2.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page2', 'Pasha', 'Nika', 'Argen'],
            'title': "My students",
        }
        return context


class Page3View(TemplateView):
    template_name = "page3.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page3', 'Pasha', 'Nika', 'Argen'],
            'title': "My students",
        }
        return context